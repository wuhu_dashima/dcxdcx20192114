
"""
作者：杜晨玄
文件名称：11
时间：2020.4.29
"""

import sqlite3
conn = sqlite3.connect('wuhu.db')
cursor = conn.cursor()

cursor.execute('create table if not exists wuhu (id, name)')
cursor.execute('insert into wuhu (id, name) values (1, "文件"), (2, "编辑"), (3, "视图"), (4, "导航")')
cursor.execute('delete from wuhu where id = 4')
cursor.execute('update wuhu set name ="运行" where id = 2')
cursor.execute('select * from wuhu')
print("数据库内容如下:\n", cursor.fetchall())
cursor.close()
conn.close()
