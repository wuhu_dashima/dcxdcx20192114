
"""
作者：杜晨玄
文件名称：3
时间：2020.3.18
"""

print("LPL热门英雄:\n")
上单 = ["铁男", "盖伦", "奥恩", "瑟提", "诺手", "剑魔","凯南"]
中单 = ["塞拉斯", "卡萨丁", "亚索",  "吸血鬼", "佐伊","卢锡安"]
打野 = ["李青", "雷克塞", "瑟庄妮", "巨魔","蜘蛛"]
ADC = ["ez", "厄斐琉斯", "女枪", "卡莎","韦鲁斯","希维尔"]
辅助 = ["悠米", "锤石", "蕾欧娜", "布隆", "派克", "泰坦"]

# 由于版本更新与部分选手赛场上出色的表现，对LPL热门英雄调整:
上单.append("复仇之矛")
上单.append("奥巴马")
中单[3] = "杰斯"
del ADC[2]
del 上单[1]
del 上单[3]

print("上单：")
for item in 上单:
    print(item, end=" ")
print("\n中单：")
for item in 中单:
    print(item, end=" ")
print("\n打野：")
for item in 打野:
    print(item, end=" ")
print("\n下路：")
for item in ADC:
    print(item, end=" ")
print("\n辅助：")
for item in 辅助:
    print(item, end=" ")