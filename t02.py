
"""
作者：杜晨玄
文件名称：7
时间：2020.4.20
"""

import math
print("欢迎使用计算器")

def add(a,b): #加
    return a + b
def sub(a,b): #减
    return a - b
def mul(a,b): #乘
    return a * b
def div(a,b): #除
    if b == 0:
        b = float(input("除数不能为零，请重新输入:"))
        return a / b
    else:
        return a/b
def mod (a,b): #模
    if b == 0:
        b = float(input("除数不能为零，请重新输入:"))
        return a % b
    else:
        return a % b
def pow(a,b): #平方
    return a ** b
def log(a,b):
    return math.log(a,b)
def sqr(a): #开方
    return math.sqrt(a)
def sin(a):
    return math.sin(a)
def cos(a):
    return math.cos(a)
def tan(a):
    return math.tan(a)

#################################

key = 1
while (key == 1):
    i = 0
    way = int(input("你想进行什么运算?\n1.add\n2.sub\n3.mul\n4.div\n5.mod\n6.pow\n7.log\n8.sqr\n9.sin\n10.cos\n11.tan\n"))
    if way == 1:
        a = float(input("请输入a:"))
        b = float(input("请输入b:"))
        print(add(a,b))
    elif way == 2:
        a = float(input("请输入a:"))
        b = float(input("请输入b:"))
        print(sub(a,b))
    elif way == 3:
        a = float(input("请输入a:"))
        b = float(input("请输入b:"))
        print(mul(a,b))
    elif way == 4:
        a = float(input("请输入a:"))
        b = float(input("请输入b:"))
        print(div(a,b))
    elif way == 5:
        a = float(input("请输入a:"))
        b = float(input("请输入b:"))
        print(mod(a,b))
    elif way == 6:
        a = float(input("请输入a:"))
        b = float(input("请输入b:"))
        print(pow(a,b))
    elif way == 7:
        a = float(input("请输入a:"))
        b = float(input("请输入b:"))
        print(log(a,b))
    elif way == 8:
        a = float(input("请输入a:"))
        print(sqr(a))
    elif way == 9:
        a = float(input("请输入a:"))
        print(sin(a))
    elif way == 10:
        a = float(input("请输入a:"))
        print(cos(a))
    elif way == 11:
        a = float(input("请输入a:"))
        print(tan(a))
    key = int(input("是否继续 (1=继续，0=不继续)"))